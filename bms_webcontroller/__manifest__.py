# -*- coding: utf-8 -*-
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Odoo Api",
    "summary": '''
        /api : page login api
        /api/auth? : method get token follow user, params: login, pw
        /api/retoken? : renew token, param: token
        /api/deltoken? : delete token, param: token
        /api/student? : show student follow parent user, param: token
        /api/student/create? : create student, param: token, name, scode
        /api/student/del? : delete student, param: token, scode
        /api/teacher/create? : create teacher, param: token, name, email
        /api/class/<id>? : get student in class have id, parameter: token
        /api/school/<id>? : get all class in school have id, param: token
        /api/groupsms? : get infomation of group send sms by teacher, param: token
    ''',
    "version": "10.0.1.2.3",
    "category": "API",
    "website": "https://",
    "author": "Enz",
    "license": "LGPL-3",
    "application": True,
    "installable": True,
    "depends": [
    ],
    "data": [
        'views/signin_api_form.xml',
        'views/vehicle_views.xml',
        'views/vehicle_managements_views.xml',
        'views/menu_views.xml'
    ],
}
