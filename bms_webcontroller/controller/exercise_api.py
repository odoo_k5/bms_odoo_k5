#coding: utf-8
from json import dumps as json
from odoo import http
from odoo.addons.web.controllers import main
from odoo import SUPERUSER_ID
from odoo.addons.web.controllers.main import serialize_exception
from random import choice
import hashlib

class ExerciseApi(http.Controller):
    @http.route('/api/get/information', type='http', auth='none', methods=['GET'], csrf=False)
    
    def api_get_information(self, **kw):
        contx = {}
        if not kw.get('token'):
            return http.request.make_response(json({
                'Message': u'parram missing'}))
        if not kw.has_key('token'):
            return http.request.make_response(json({
                'Message': u'Cần nhập vào token'}))
        user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
        if not user_id:
            http.request.make_response(json({
                'Message': u'Token không chính xác. Vui lòng kiểm tra lại'}))
        res_partner = http.request.env['res.partner'].sudo().search([('id', '=', user_id.partner_id.id)])
        if not res_partner:
            return http.request.make_response(json({
                'Message': u'error'}))
        contx = {
            'name' : res_partner.name,
            'phone' : res_partner.phone,
            'email' : res_partner.email
        }
        return http.request.make_response(json(contx))


    @http.route('/api/create/user', type='http', auth='none', methods=['POST'], csrf=False)
    def api_create_user(self, **kw):
        if not kw.get('email') and kw.get('pw') and kw.get('name'):
            return http.request.make_response(json({
                'Message': u'parram missing'}))
        name = kw.get('name')
        email = kw.get('email')
        pw = kw.get('pw')
        if pw:
            pw = hashlib.md5(pw).hexdigest()
        try:
            res = http.request.env['res.users'].sudo().create({'name': name, 'login': email, 'password': pw})
        except:
            return http.request.make_response(json({
                'Status': 403, 'Permission': 'Denied'}),
                [('Access-Control-Allow-Origin', '*')])
        res_json = json({'Status': 200, 'Create': 'Success'})
        return http.request.make_response(
            res_json, [('Access-Control-Allow-Origin', '*')])

    @http.route('/api/delete/user', type='http', auth='none', methods=['POST'], csrf=False)

    def api_delete_user(self, **kw):

        if not kw.get('id'):
            return http.request.make_response(json({
                'Message': u'parram missing'}))
        id = kw.get('id')
        try:
            res = http.request.env['res.users'].sudo().browse(int(id)).unlink()
        except:
            return http.request.make_response(json({
                'Status': 403, 'Permission': 'Denied'}),
                [('Access-Control-Allow-Origin', '*')])
        res_json = json({'Status': 200, 'Delete': 'Success'})
        return http.request.make_response(
            res_json, [('Access-Control-Allow-Origin', '*')])
