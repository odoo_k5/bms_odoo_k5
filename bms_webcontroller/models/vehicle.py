# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
from odoo import fields, models


class Vehicle(models.Model):
    _name = 'vehicle'

    
    license_plate = fields.Char(u'Biển số')

    empty_weight = fields.Float(u'Trọng lượng rỗng', default=0)