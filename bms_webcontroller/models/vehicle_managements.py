# -*- coding: utf-8 -*-
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
from odoo import fields, models


class VehicleManagements(models.Model):
    _name = 'vehicle.managements'

    
    vehicle_id = fields.Many2one('vehicle', u'vehicleID', required=True)
    check_in_time = fields.Datetime(u'Thời gian vào ra')
    check_in = fields.Boolean(u'Vào ra')
    weight = fields.Float(u'Trọng lượng', default=0)